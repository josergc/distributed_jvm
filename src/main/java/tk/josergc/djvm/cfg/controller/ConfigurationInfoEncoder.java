package tk.josergc.djvm.cfg.controller;

import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import tk.josergc.djvm.cfg.model.ConfigurationInfo;

public class ConfigurationInfoEncoder {
	public static void encode(ConfigurationInfo configurationInfo, OutputStream os) throws JAXBException {
		JAXBContext
			.newInstance(ConfigurationInfo.class.getName())
			.createMarshaller()
			.marshal(configurationInfo,os)
			;
	}
}
