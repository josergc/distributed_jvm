package tk.josergc.djvm.cfg.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import tk.josergc.djvm.cfg.model.ConfigurationInfo;

public class ConfigurationInfoDecoder {
	
	/**
	 * Decodes the configuration encoded in an XML document.
	 * <a href="http://download.oracle.com/docs/cd/E17409_01/javase/7/docs/api/javax/xml/bind/UnmarshallerHandler.html">Reference</a>
	 * @param is
	 * @return the configuration 
	 * */
	public ConfigurationInfo decode(InputStream is) throws JAXBException, SAXException, ParserConfigurationException, IOException {
		JAXBContext context = JAXBContext.newInstance(ConfigurationInfo.class.getName());

       Unmarshaller unmarshaller = context.createUnmarshaller();

       UnmarshallerHandler unmarshallerHandler = unmarshaller.getUnmarshallerHandler();

       SAXParserFactory spf = SAXParserFactory.newInstance();
       spf.setNamespaceAware(true);

       XMLReader xmlReader = spf.newSAXParser().getXMLReader();
       xmlReader.setContentHandler(unmarshallerHandler);
       xmlReader.parse(new InputSource(is));

	   return (ConfigurationInfo)unmarshallerHandler.getResult();
	}
}
