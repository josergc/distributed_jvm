package tk.josergc.djvm.ui.cmd.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import tk.josergc.djvm.ui.cmd.model.Configuration;

public final class ConfigurationDecoder implements ContentHandler {
	
	
	
	private Configuration cfg;
	private Stack<Object> stack = new Stack<Object>();

	private ConfigurationDecoder(Configuration cfg) {
		this.cfg = cfg;
	}
	
	/**
	 * @throws SAXException 
	 * @throws IOException 
	 */
	public static Configuration decode(String filePath) throws SAXException, IOException {
		Configuration cfg = new Configuration();
		XMLReader reader = XMLReaderFactory.createXMLReader();
		reader.setContentHandler(new ConfigurationDecoder(cfg));
		InputStream is;
		try {
			is = URI.create(filePath).toURL().openStream();
		} catch(Throwable e) {
			is = new FileInputStream(filePath);
		}
		try {
			reader.parse(new InputSource(is));
		} finally {
			is.close();
		}
		return cfg;
	}

	@Override
	public void characters(char[] arg0, int arg1, int arg2) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endDocument() throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endElement(String arg0, String arg1, String arg2)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void endPrefixMapping(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void ignorableWhitespace(char[] arg0, int arg1, int arg2)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void processingInstruction(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void setDocumentLocator(Locator arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void skippedEntity(String arg0) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startDocument() throws SAXException {
		
	}

	@Override
	public void startElement(String arg0, String arg1, String arg2, Attributes arg3) throws SAXException {
		// TODO Auto-generated method stub

	}

	@Override
	public void startPrefixMapping(String arg0, String arg1)
			throws SAXException {
		// TODO Auto-generated method stub

	}

}
