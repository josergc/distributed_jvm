package tk.josergc.djvm.ui.cmd.model;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Server {
	public final String host;
	public final int port;
	private Socket socket;
	public Server(String host, int port) {
		this.host = host;
		this.port = port;
	}
	/**
	 * @return Returns the socket associated with this server. Creates the 
	 * connection if it is the first attempt 
	 * @throws IOException 
	 * @throws UnknownHostException 
	 */
	public Socket getSocket() throws UnknownHostException, IOException {
		if (socket == null) {
			socket = new Socket(host,port);
		}
		return socket;
	}
}
