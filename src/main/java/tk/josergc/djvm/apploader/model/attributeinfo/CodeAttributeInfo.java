package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.ExceptionTable;

public class CodeAttributeInfo extends AttributeInfo {

	public static final String NAME = "Code";
	

	public final int maxStack;
	public final int maxLocals; 
	public final byte[] code;
	public final ExceptionTable[] exceptionTable; 
	public final AttributeInfo[] attributes;
	
	public CodeAttributeInfo(
			ConstantPoolInfo[] constantPool,
			int attributeNameIndex, 
			byte[] info, 
			int maxStack, 
			int maxLocals, 
			byte[] code, 
			ExceptionTable[] exceptionTable, 
			AttributeInfo[] attributes
			) {
		super(constantPool, attributeNameIndex, info);
		this.maxStack = maxStack;
		this.maxLocals = maxLocals;
		this.code = code;
		this.exceptionTable = exceptionTable;
		this.attributes = attributes;
	}

}
