package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Float_info extends ConstantPoolInfo {

	public final static int TAG = 4;
	public final float bytes;
	
	public CONSTANT_Float_info(ConstantPoolInfo[] constantPool, float bytes) {
		super(constantPool,TAG);		
		this.bytes = bytes;
	}

}
