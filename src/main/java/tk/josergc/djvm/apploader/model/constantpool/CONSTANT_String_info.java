package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_String_info extends ConstantPoolInfo {

	public final static int TAG = 8;
	public final int stringIndex;
	
	public CONSTANT_String_info(ConstantPoolInfo[] constantPool, int stringIndex) {
		super(constantPool,TAG);		
		this.stringIndex = stringIndex;
	}

}
