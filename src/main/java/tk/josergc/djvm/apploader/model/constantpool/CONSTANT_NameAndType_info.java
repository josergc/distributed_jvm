package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_NameAndType_info extends ConstantPoolInfo {

	public static final int TAG = 12;
	public final int nameAndTypeIndex;
	public final int descriptorIndex;

	public CONSTANT_NameAndType_info(ConstantPoolInfo[] constantPool,
			int nameAndTypeIndex, int descriptorIndex) {
		super(constantPool,TAG);
		this.nameAndTypeIndex = nameAndTypeIndex;
		this.descriptorIndex = descriptorIndex;
	}

}
