package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Integer_info extends ConstantPoolInfo {

	public final static int TAG = 3;
	public final int bytes;
	
	public CONSTANT_Integer_info(ConstantPoolInfo[] constantPool, int bytes) {
		super(constantPool,TAG);		
		this.bytes = bytes;
	}

}
