package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Double_info extends ConstantPoolInfo {

	public final static int TAG = 6;
	public final double bytes;

	public CONSTANT_Double_info(ConstantPoolInfo[] constantPool, double bytes) {
		super(constantPool,TAG);		
		this.bytes = bytes;
	}

}
