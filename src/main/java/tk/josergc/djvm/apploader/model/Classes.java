package tk.josergc.djvm.apploader.model;

public class Classes {

	public final ConstantPoolInfo[] constantPool;
	public final int innerClassInfoIndex;
	public final int outerClassInfoIndex;
	public final int innerNameIndex;
	public final int innerClassAccessFlags;

	public Classes(ConstantPoolInfo[] constantPool, int innerClassInfoIndex, int outerClassInfoIndex, int innerNameIndex, int innerClassAccessFlags) {
		this.constantPool = constantPool;
		this.innerClassInfoIndex = innerClassInfoIndex;
		this.outerClassInfoIndex = outerClassInfoIndex;
		this.innerNameIndex = innerNameIndex;
		this.innerClassAccessFlags = innerClassAccessFlags;
	}

}
