package tk.josergc.djvm.apploader.model;

import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Utf8_info;

public class MethodInfo {
	public final ConstantPoolInfo[] constantPool; 
	public final int accessFlags;
	public final int nameIndex;
	public final int descriptorIndex;
	public final AttributeInfo[] attributeInfo;

	public MethodInfo(
		ConstantPoolInfo[] constantPool, 
		int accessFlags,
		int nameIndex, 
		int descriptorIndex, 
		AttributeInfo[] attributeInfo
		) {
		this.constantPool = constantPool;
		this.accessFlags = accessFlags;
		this.nameIndex = nameIndex;
		this.descriptorIndex = descriptorIndex;
		this.attributeInfo = attributeInfo;
	}
	
	public boolean isAccPublic() {
		return (accessFlags & 0x0001) != 0;
	}

	public boolean isAccPrivate() {
		return (accessFlags & 0x0002) != 0;
	}

	public boolean isAccProtected() {
		return (accessFlags & 0x0004) != 0;
	}

	public boolean isAccStatic() {
		return (accessFlags & 0x0008) != 0;
	}

	public boolean isAccFinal() {
		return (accessFlags & 0x0010) != 0;
	}

	public boolean isAccSynchronized() {
		return (accessFlags & 0x0020) != 0;
	}

	public boolean isAccNative() {
		return (accessFlags & 0x0100) != 0;
	}

	public boolean isAccAbstract() {
		return (accessFlags & 0x0400) != 0;
	}
	
	public boolean isAccStrict() {
		return (accessFlags & 0x0800) != 0;
	}
	
	public String getName() {
		return ((CONSTANT_Utf8_info)constantPool[nameIndex]).bytes;
	}

	public String getDescriptor() {
		return ((CONSTANT_Utf8_info)constantPool[descriptorIndex]).bytes;
	}
}
