package tk.josergc.djvm.apploader.model;

public class LineNumberTable {

	public final ConstantPoolInfo[] constantPool;
	public final int startPc;
	public final int lineNumber;

	public LineNumberTable(ConstantPoolInfo[] constantPool, int startPc,
			int lineNumber) {
		this.constantPool = constantPool;
		this.startPc = startPc;
		this.lineNumber = lineNumber;
	}

}
