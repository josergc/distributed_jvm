package tk.josergc.djvm.apploader.model;

public class AttributeInfo {
	public final ConstantPoolInfo[] constantPool;
	public final int attributeNameIndex;
	public final byte[] info;
	
	public AttributeInfo(
		ConstantPoolInfo[] constantPool,
		int attributeNameIndex,
		byte[] info
		) {
		this.constantPool = constantPool;
		this.attributeNameIndex = attributeNameIndex;
		this.info = info;
	}

}
