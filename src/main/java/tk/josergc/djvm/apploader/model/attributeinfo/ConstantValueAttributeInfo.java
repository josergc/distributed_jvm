package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class ConstantValueAttributeInfo extends AttributeInfo {
	
	public static final String NAME = "ConstantValue"; 

	public final int constantValueIndex;

	public ConstantValueAttributeInfo(
		ConstantPoolInfo[] constantPool,
		int attributeNameIndex, 
		byte[] info,
		int constantValueIndex
		) {
		super(constantPool,attributeNameIndex,info);
		this.constantValueIndex = constantValueIndex;
	}
	
	

}
