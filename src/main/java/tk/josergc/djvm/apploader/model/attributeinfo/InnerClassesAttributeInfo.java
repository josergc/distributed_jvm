package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.Classes;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class InnerClassesAttributeInfo extends AttributeInfo {

	public static final String NAME = "InnerClasses";
	public final Classes[] classes;

	public InnerClassesAttributeInfo(ConstantPoolInfo[] constantPool,
			int attributeNameIndex, byte[] info, Classes[] classes) {
		super(constantPool, attributeNameIndex, info);
		this.classes = classes;
	}

}
