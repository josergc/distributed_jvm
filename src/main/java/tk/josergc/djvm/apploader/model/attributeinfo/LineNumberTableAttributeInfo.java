package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.LineNumberTable;

public class LineNumberTableAttributeInfo extends AttributeInfo {

	public static final String NAME = "LineNumberTable";
	public final LineNumberTable[] lineNumberTable;

	public LineNumberTableAttributeInfo(ConstantPoolInfo[] constantPool,
			int attributeNameIndex, byte[] info, LineNumberTable[] lineNumberTable) {
		super(constantPool, attributeNameIndex, info);
		this.lineNumberTable = lineNumberTable;
	}

}
