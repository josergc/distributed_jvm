package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_InterfaceMethodref_info extends ConstantPoolInfo {

	public final static int TAG = 11;
	public final int classIndex;
	public final int nameAndTypeIndex;
	
	public CONSTANT_InterfaceMethodref_info(ConstantPoolInfo[] constantPool,
			int classIndex, int nameAndTypeIndex) {
		super(constantPool,TAG);		
		this.classIndex = classIndex;
		this.nameAndTypeIndex = nameAndTypeIndex;
	}

}
