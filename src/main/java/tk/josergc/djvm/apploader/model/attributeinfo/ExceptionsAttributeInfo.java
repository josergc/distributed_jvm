package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class ExceptionsAttributeInfo extends AttributeInfo {

	public static final String NAME = "Exceptions";
	public final int[] exceptionIndexTable;

	public ExceptionsAttributeInfo(ConstantPoolInfo[] constantPool,
			int attributeNameIndex, byte[] info, int[] exceptionIndexTable) {
		super(constantPool, attributeNameIndex, info);
		this.exceptionIndexTable = exceptionIndexTable;
		
	}

}
