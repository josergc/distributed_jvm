package tk.josergc.djvm.apploader.model;


public class ClassDescriptor {

	public final int minorVersion;
	public final int majorVersion;
	public final ConstantPoolInfo[] constantPool;
	public final int accessFlags;
	public final int thisClass;
	public final int superClass;
	public final Interface[] interfaces;
	public final FieldInfo[] fields;
	public final MethodInfo[] methods;
	public final AttributeInfo[] attributes;

	public ClassDescriptor(int minorVersion, int majorVersion,
			ConstantPoolInfo[] constantPool, int accessFlags, int thisClass,
			int superClass, Interface[] interfaces, FieldInfo[] fields, 
			MethodInfo[] methods,
			AttributeInfo[] attributes) {
		this.minorVersion = minorVersion;
		this.majorVersion = majorVersion;
		this.constantPool = constantPool;
		this.accessFlags = accessFlags;
		this.thisClass = thisClass;
		this.superClass = superClass;
		this.interfaces = interfaces;
		this.fields = fields;
		this.methods = methods;
		this.attributes = attributes;
	}

}
