package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Class_info extends ConstantPoolInfo {

	public final static int TAG = 7;
	public final int nameIndex;
	
	public CONSTANT_Class_info(ConstantPoolInfo[] constantPool, int nameIndex) {
		super(constantPool,TAG);		
		this.nameIndex = nameIndex;
	}

}
