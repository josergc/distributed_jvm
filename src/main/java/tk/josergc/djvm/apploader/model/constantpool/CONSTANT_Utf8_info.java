package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Utf8_info extends ConstantPoolInfo {

	public static final int TAG = 1;
	public final String bytes;

	public CONSTANT_Utf8_info(ConstantPoolInfo[] constantPool, String bytes) {
		super(constantPool,TAG);
		this.bytes = bytes;
	}

}
