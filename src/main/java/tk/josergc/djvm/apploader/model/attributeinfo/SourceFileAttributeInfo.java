package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class SourceFileAttributeInfo extends AttributeInfo {

	public static final String NAME = "SourceFile";
	public final int sourcefileIndex;

	public SourceFileAttributeInfo(ConstantPoolInfo[] constantPool,
			int attributeNameIndex, byte[] info, int sourcefileIndex) {
		super(constantPool, attributeNameIndex, info);
		this.sourcefileIndex = sourcefileIndex;
	}

}
