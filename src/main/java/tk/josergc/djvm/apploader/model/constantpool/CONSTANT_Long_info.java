package tk.josergc.djvm.apploader.model.constantpool;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class CONSTANT_Long_info extends ConstantPoolInfo {

	public final static int TAG = 5;
	public final long bytes;

	public CONSTANT_Long_info(ConstantPoolInfo[] constantPool, long bytes) {
		super(constantPool,TAG);		
		this.bytes = bytes;
	}

}
