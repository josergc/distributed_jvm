package tk.josergc.djvm.apploader.model.attributeinfo;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class SyntheticAttributeInfo extends AttributeInfo {

	public static final String NAME = "Synthetic";

	public SyntheticAttributeInfo(ConstantPoolInfo[] constantPool,
			int attributeNameIndex, byte[] info) {
		super(constantPool, attributeNameIndex, info);
	}

}
