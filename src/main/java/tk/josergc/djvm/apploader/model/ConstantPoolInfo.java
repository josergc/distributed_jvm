package tk.josergc.djvm.apploader.model;

public class ConstantPoolInfo {

	public final ConstantPoolInfo[] constantPool;
	public final int tag;
	
	public ConstantPoolInfo(ConstantPoolInfo[] constantPool, int tag) {
		this.constantPool = constantPool;
		this.tag = tag;
	}
	
}
