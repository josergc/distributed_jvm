package tk.josergc.djvm.apploader.model;

public class ExceptionTable {

	public final ConstantPoolInfo[] constantPool; 
	public final int startPc;
	public final int endPc;
	public final int handlerPc; 
	public final int catchType;
	
	public ExceptionTable(
		ConstantPoolInfo[] constantPool, 
		int startPc,
		int endPc, 
		int handlerPc, 
		int catchType
		) {
		this.constantPool = constantPool;
		this.startPc = startPc;
		this.endPc = endPc;
		this.handlerPc = handlerPc;
		this.catchType = catchType;
	}

}
