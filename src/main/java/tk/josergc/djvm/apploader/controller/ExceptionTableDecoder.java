package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.ExceptionTable;

public class ExceptionTableDecoder {

	public static ExceptionTable decode(
		ConstantPoolInfo[] constantPool,
		DataInputStream dis
		) throws IOException {
		int startPc = dis.readUnsignedShort();
		int endPc = dis.readUnsignedShort();
		int handlerPc = dis.readUnsignedShort();
		int catchType = dis.readUnsignedShort();
		return new ExceptionTable(
			constantPool,
			startPc,
			endPc,
			handlerPc,
			catchType
			);
	}

}
