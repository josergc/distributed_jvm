package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import tk.josergc.djvm.apploader.controller.utils.MagicNumberNotFoundException;
import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ClassDescriptor;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.FieldInfo;
import tk.josergc.djvm.apploader.model.Interface;
import tk.josergc.djvm.apploader.model.MethodInfo;

public class ClassDecoder {
	public static ClassDescriptor decode(InputStream is) throws IOException, MagicNumberNotFoundException {
		DataInputStream dis = new DataInputStream(is);
		
		if (dis.readInt() != 0xcafebabe)
			throw new MagicNumberNotFoundException();
		
		int minorVersion = dis.readUnsignedShort();
		int majorVersion = dis.readUnsignedShort();
		ConstantPoolInfo[] constantPool = new ConstantPoolInfo[dis.readUnsignedShort()];
		for (int i = 1, fi = constantPool.length; i < fi; i++)
			constantPool[i] = ConstantPoolInfoDecoder.decode(constantPool,dis);
		int accessFlags = dis.readUnsignedShort();
		int thisClass = dis.readUnsignedShort();
		int superClass = dis.readUnsignedShort();
		Interface[] interfaces = new Interface[dis.readUnsignedShort()];
		for (int i = 0, fi = interfaces.length; i < fi; i++)
			interfaces[i] = InterfaceDecoder.decode(constantPool,dis);
		FieldInfo[] fields = new FieldInfo[dis.readUnsignedShort()];
		for (int i = 0, fi = fields.length; i < fi; i++)
			fields[i] = FieldInfoDecoder.decode(constantPool,dis);
		MethodInfo[] methods = new MethodInfo[dis.readUnsignedShort()];
		for (int i = 0, fi = methods.length; i < fi; i++)
			methods[i] = MethodInfoDecoder.decode(constantPool,dis);
		AttributeInfo[] attributes = new AttributeInfo[dis.readUnsignedShort()];
		for (int i = 0, fi = attributes.length; i < fi; i++)
			attributes[i] = AttributeInfoDecoder.decode(constantPool,dis);
		
		return new ClassDescriptor(
				minorVersion,
				majorVersion,
				constantPool,
				accessFlags,
				thisClass,
				superClass,
				interfaces,
				fields,
				methods,
				attributes
				);
	} 
}
