package tk.josergc.djvm.apploader.controller;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.Classes;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.ExceptionTable;
import tk.josergc.djvm.apploader.model.LineNumberTable;
import tk.josergc.djvm.apploader.model.attributeinfo.CodeAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.ConstantValueAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.ExceptionsAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.InnerClassesAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.LineNumberTableAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.SourceFileAttributeInfo;
import tk.josergc.djvm.apploader.model.attributeinfo.SyntheticAttributeInfo;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Utf8_info;

public class AttributeInfoDecoder {

	public static AttributeInfo decode(ConstantPoolInfo[] constantPool,
			DataInputStream dis) throws IOException {
		// Reads the common fields of the AttributeInfo objects
		int attributeNameIndex = dis.readUnsignedShort();
		int attributeLength = dis.readInt();
		byte[] info = new byte[attributeLength];
		dis.readFully(info);
		
		// Tries to determine the kind of AttributeInfo object
		dis = new DataInputStream(new ByteArrayInputStream(info));
		String attributeName = ((CONSTANT_Utf8_info)constantPool[attributeNameIndex]).bytes;
		if (ConstantValueAttributeInfo.NAME.equals(attributeName)) {
			int constantValueindex = dis.readUnsignedShort();
			return new ConstantValueAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				constantValueindex
				);
			
		}
		if (CodeAttributeInfo.NAME.equals(attributeName)) {
			int maxStack = dis.readUnsignedShort();
			int maxLocals = dis.readUnsignedShort();
			int codeLength = dis.readInt();
			byte[] code = new byte[codeLength];
			dis.readFully(code);
			int exceptionTableLength = dis.readUnsignedShort();
			ExceptionTable[] exceptionTable = new ExceptionTable[exceptionTableLength];
			for (int i = 0; i < exceptionTableLength; i++) 
				exceptionTable[i] = ExceptionTableDecoder.decode(constantPool,dis);
			int attributesCount = dis.readUnsignedShort();
			AttributeInfo[] attributes = new AttributeInfo[attributesCount]; 
			for (int i = 0; i < attributesCount; i++)
				attributes[i] = AttributeInfoDecoder.decode(constantPool,dis);
			return new CodeAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				maxStack,
				maxLocals,
				code,
				exceptionTable,
				attributes
				);
			
		}
		if (ExceptionsAttributeInfo.NAME.equals(attributeName)) {
			int numberOfExceptions = dis.readUnsignedShort();
			int[] exceptionIndexTable = new int[numberOfExceptions];
			for (int i = 0; i < numberOfExceptions; i++)
				exceptionIndexTable[i] = dis.readUnsignedShort();
			return new ExceptionsAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				exceptionIndexTable
				);
		}
		if (InnerClassesAttributeInfo.NAME.equals(attributeName)) {
			int numberOfClasses = dis.readUnsignedShort();
			Classes[] classes = new Classes[numberOfClasses];
			for (int i = 0; i < numberOfClasses; i++)
				classes[i] = ClassesDecoder.decode(constantPool,dis);
			return new InnerClassesAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				classes
				);
		}
		if (SyntheticAttributeInfo.NAME.equals(attributeName)) {
			return new SyntheticAttributeInfo(
				constantPool,
				attributeNameIndex,
				info
				);
		}
		if (SourceFileAttributeInfo.NAME.equals(attributeName)) {
			int sourcefileIndex = dis.readUnsignedShort();
			return new SourceFileAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				sourcefileIndex
				);
		}
		if (LineNumberTableAttributeInfo.NAME.equals(attributeName)) {
			int lineNumberTableLength = dis.readUnsignedShort();
			LineNumberTable[] lineNumberTable = new LineNumberTable[lineNumberTableLength];
			for (int i = 0; i < lineNumberTableLength; i++)
				lineNumberTable[i] = LineNumberTableDecoder.decode(constantPool,dis); 
			return new LineNumberTableAttributeInfo(
				constantPool,
				attributeNameIndex,
				info,
				lineNumberTable
				);
		}
		return new AttributeInfo(
			constantPool,
			attributeNameIndex,
			info
			);
	}

}
