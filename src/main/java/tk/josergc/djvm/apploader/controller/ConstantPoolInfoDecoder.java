package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Class_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Double_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Fieldref_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Float_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Integer_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_InterfaceMethodref_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Long_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Methodref_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_NameAndType_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_String_info;
import tk.josergc.djvm.apploader.model.constantpool.CONSTANT_Utf8_info;

public class ConstantPoolInfoDecoder {

	public static ConstantPoolInfo decode(ConstantPoolInfo[] constantPool, DataInputStream dis) throws IOException {
		int tag = dis.readUnsignedByte();
		switch(tag) {
			case CONSTANT_Class_info.TAG: {
				int nameIndex = dis.readUnsignedShort();
				return new CONSTANT_Class_info(constantPool,nameIndex);
			}
			case CONSTANT_Fieldref_info.TAG: {
				int classIndex = dis.readUnsignedShort();
				int nameAndTypeIndex = dis.readUnsignedShort();
				return new CONSTANT_Fieldref_info(constantPool,classIndex,nameAndTypeIndex);
			}
			case CONSTANT_Methodref_info.TAG: {
				int classIndex = dis.readUnsignedShort();
				int nameAndTypeIndex = dis.readUnsignedShort();
				return new CONSTANT_Methodref_info(constantPool,classIndex,nameAndTypeIndex);
			}
			case CONSTANT_InterfaceMethodref_info.TAG: {
				int classIndex = dis.readUnsignedShort();
				int nameAndTypeIndex = dis.readUnsignedShort();
				return new CONSTANT_InterfaceMethodref_info(constantPool,classIndex,nameAndTypeIndex);
			}
			case CONSTANT_String_info.TAG: {
				int stringIndex = dis.readUnsignedShort();
				return new CONSTANT_String_info(constantPool,stringIndex);
			}
			case CONSTANT_Integer_info.TAG: {
				int bytes = dis.readInt();
				return new CONSTANT_Integer_info(constantPool,bytes);
			}
			case CONSTANT_Float_info.TAG: {
				float bytes = dis.readFloat();
				return new CONSTANT_Float_info(constantPool,bytes);
			}
			case CONSTANT_Long_info.TAG: {
				long bytes = dis.readLong();
				return new CONSTANT_Long_info(constantPool,bytes);
			}
			case CONSTANT_Double_info.TAG: {
				double bytes = dis.readDouble();
				return new CONSTANT_Double_info(constantPool,bytes);
			}
			case CONSTANT_NameAndType_info.TAG: {
				int nameAndTypeIndex = dis.readUnsignedShort();
				int descriptorIndex = dis.readUnsignedShort();
				return new CONSTANT_NameAndType_info(constantPool,nameAndTypeIndex,descriptorIndex);
			}
			case CONSTANT_Utf8_info.TAG: {
				String bytes = dis.readUTF();
				return new CONSTANT_Utf8_info(constantPool,bytes);
			}
			default:
		}
		return new ConstantPoolInfo(constantPool,tag);
	}

}
