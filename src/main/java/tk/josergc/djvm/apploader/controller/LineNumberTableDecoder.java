package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.LineNumberTable;

public class LineNumberTableDecoder {

	public static LineNumberTable decode(ConstantPoolInfo[] constantPool,
			DataInputStream dis) throws IOException {
		int startPc = dis.readUnsignedShort();
		int lineNumber = dis.readUnsignedShort();		
		return new LineNumberTable(constantPool,startPc,lineNumber);
	}

}
