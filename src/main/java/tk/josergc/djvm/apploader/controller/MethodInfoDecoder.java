package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.AttributeInfo;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;
import tk.josergc.djvm.apploader.model.FieldInfo;
import tk.josergc.djvm.apploader.model.MethodInfo;

public class MethodInfoDecoder {

	public static MethodInfo decode(ConstantPoolInfo[] constantPool, DataInputStream dis) throws IOException {
		int accessFlags = dis.readUnsignedShort();
		int nameIndex = dis.readUnsignedShort();
		int descriptorIndex = dis.readUnsignedShort();
		int attributesCount = dis.readUnsignedShort();
		AttributeInfo[] attributeInfo = new AttributeInfo[attributesCount];
		for (int i = 0; i < attributesCount; i++)
			attributeInfo[i] = AttributeInfoDecoder.decode(constantPool, dis);
		return new MethodInfo(
			constantPool,
			accessFlags,
			nameIndex,
			descriptorIndex,
			attributeInfo
			);
	}

}
