package tk.josergc.djvm.apploader.controller;

import java.io.DataInputStream;
import java.io.IOException;

import tk.josergc.djvm.apploader.model.Classes;
import tk.josergc.djvm.apploader.model.ConstantPoolInfo;

public class ClassesDecoder {

	public static Classes decode(ConstantPoolInfo[] constantPool,
			DataInputStream dis) throws IOException {
		int innerClassInfoIndex = dis.readUnsignedShort();
		int outerClassInfoIndex = dis.readUnsignedShort();
		int innerNameIndex = dis.readUnsignedShort();
		int innerClassAccessFlags = dis.readUnsignedShort();
		return new Classes(constantPool,innerClassInfoIndex,outerClassInfoIndex,innerNameIndex,innerClassAccessFlags);
	}

}
